//TMP36 Pin Variables
int sensorPin = 0; //the analog pin the TMP36's Vout (sense) pin is connected to
                  
const int ledPin = 9;       // pin that the LED is attached to
const int threshold = 27;   // an arbitrary threshold level that's in the range of the analog input

void setup() {
  // initialize the LED pin as an output:
  pinMode(ledPin, OUTPUT);
  // initialize serial communications:
  Serial.begin(9600);
}
 
void loop()                     // run over and over again
{
 
 int reading = analogRead(sensorPin);  
 
 float voltage = reading * 5.0;
 voltage /= 1024.0; 
 
 // now print out the temperature
 float temperatureC = (voltage - 0.5) * 100 ;  //converting from 10 mv per degree wit 500 mV offset
                                               //to degrees ((voltage - 500mV) times 100)
 Serial.print(temperatureC); Serial.println(" degrees C");
 

  // if the analog value is high enough, turn on the LED:
  if (temperatureC > threshold) {
   tone(ledPin, 1000);
  } else {
   noTone(ledPin);
  }
  delay(1000);                                     //waiting a second
 }
